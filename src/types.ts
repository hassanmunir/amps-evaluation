export type Release = {
  id: string
  state: string
  tradeSide: string
  fillQuantity: number
  releaseQuantity: number
  bookedQuantity: number
  isViaFix: boolean
  isHidden: boolean
}

export type Filter = [string, string, string | number]

export type Query = {
  readonly topic: string
  readonly groupBy?: string[]
  readonly filter?: Filter[]
}

export type DataRecord<T> = DataRecordTracking & T

export type DataRecordTracking = {
  readonly __key: string
  readonly __path: string[]
}

export type Transaction<T> = {
  readonly add?: DataRecord<T>[]
  readonly update?: DataRecord<T>[]
  readonly remove?: { __key: string }[]
}

export type Reset = {
  readonly __reset?: true
}

export type DataTransaction<T> = Reset & Transaction<T>
