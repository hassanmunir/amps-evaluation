import { KafkaClient, Producer, Consumer } from 'kafka-node'
import uuid from 'uuid/v4'

import { Release } from './types'

const INTERVAL = 1000
const ADD_NEW = true
const ADD_PROBABILITY = 0.5

const DELETE = true
const DELETE_PROBABILITY = 1

const TOPIC = 'releases'
const IDX_COLUMN = 'id'

const kafkaHost = 'localhost:9092'

const client = new KafkaClient({ kafkaHost })
const producer = new Producer(client)
const consumer = new Consumer(client, [{ topic: TOPIC }], {
  autoCommit: false,
  fromOffset: false,
})

const items = new Map()

consumer.on('message', (msg) => {
  try {
    if (msg.value === null) {
      items.delete(msg.key)
    } else {
      const data = JSON.parse(msg.value as string)
      items.set(msg.key, data)
    }
  } catch (err) {
    console.warn(err)
  }
})

const sample: Release = {
  id: 'c957bdf421c449fdb9f88b4d9dd24943',
  bookedQuantity: 42,
  fillQuantity: 84,
  releaseQuantity: 126,
  isHidden: true,
  isViaFix: true,
  state: 'Released',
  tradeSide: 'Buy',
}

const tradeSides = ['Sell', 'Buy']
const states = ['Released', 'Cancelled']

const random = () => Math.random()

const heads = () => random() < 0.5

const zeroOrOne = () => (random() < 0.5 ? 0 : 1)

const randomInt = (max: number) => Math.floor(random() * max)

const number = () => (random() * 100).toPrecision(2)

items.set(sample[IDX_COLUMN], sample)

const mutate = (seed: Release, id: string) => ({
  ...seed,
  id,
  bookedQuantity: number(),
  fillQuantity: number(),
  releaseQuantity: number(),
  isViaFix: heads() ? true : false,
  state: states[zeroOrOne()],
  tradeSide: tradeSides[zeroOrOne()],
})

const newRelease = () => mutate(sample, uuid())

const existingRelease = () => {
  const idx = randomInt(items.size)
  // console.log(`Modifying item ${idx}`)

  const t = Array.from(items)[idx][1]
  if (t === null) return existingRelease()
  else return t
}

const updateRelease = () => {
  const e = existingRelease()
  return mutate(e, e[IDX_COLUMN])
}

const next = () =>
  ADD_NEW && random() < ADD_PROBABILITY ? newRelease() : updateRelease()

const send = (key: string, msg: any) =>
  producer.send(
    [
      {
        topic: TOPIC,
        key,
        messages: JSON.stringify(msg),
      },
    ],
    (err) => err && console.error('error', err)
  )

setInterval(() => {
  const n = next()
  if (DELETE && random() < DELETE_PROBABILITY) {
    send(n[IDX_COLUMN], null)
  } else {
    send(n[IDX_COLUMN], n)
  }
}, INTERVAL)
