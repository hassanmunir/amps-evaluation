import { Client, Command, Message } from 'amps'
import { Observable } from 'rxjs'

import { Query, Transaction } from './types'

export type DataRecord<T> = WithKey & T

type WithKey = {
  readonly __key: string
  readonly __path: string[]
}

type Mutable<T> = {
  -readonly [P in keyof T]: T[P] extends ReadonlyArray<infer U>
    ? Mutable<U>[]
    : Mutable<T[P]>
}

const configureCommand = (cmd: Command, query: Query) => {
  const options = `oof,conflation=1000ms`

  cmd.topic(query.topic)

  if (query.filter) {
    //todo append filter
  } else if (query.groupBy?.length) {
    // todo  enforce single group
    // todo - compute projection
    const groupBy = query.groupBy[0]

    cmd.options(`
    ${options},
      grouping=[/${groupBy}]
      projection=[
        /${groupBy}
      ]`)
  }
}

const snapshotAndSubscribe = <T>(client: Client, query: Query) => {
  const cmd = new Command('sow_and_subscribe')
  configureCommand(cmd, query)
  return subscription<T>(client, cmd, (data) => [data.__key])
}

const subscription = <T>(
  client: Client,
  cmd: Command,
  pathFinder: (r: DataRecord<T>) => string[]
) => {
  return new Observable<Transaction<T>>((observer) => {
    const keys = new Set<string>()

    let sowRows: DataRecord<T>[]

    let subscriptionId: string

    client
      .execute(cmd, (message: Message) => {
        const data: T & Mutable<WithKey> = message.data

        switch (message.header.command()) {
          case 'group_begin':
            sowRows = []
            break
          case 'sow':
            if (data == null) break
            data.__key = message.header.sowKey()
            data.__path = pathFinder(data)
            sowRows.push(data)
            keys.add(data.__key)
            break
          case 'group_end':
            observer.next({ add: sowRows })
            break
          case 'oof':
            keys.delete(message.header.sowKey())
            observer.next({ remove: [{ __key: message.header.sowKey() }] })
            break
          default:
            if (data == null) break
            data.__key = message.header.sowKey()
            data.__path = pathFinder(data)
            if (keys.has(data.__key)) {
              observer.next({ update: [data] })
            } else {
              observer.next({ add: [data] })
            }
        }
      })
      .then((subId) => (subscriptionId = subId))
      .catch((err) => observer.error(err))

    return () =>
      subscriptionId &&
      client
        .unsubscribe(subscriptionId)
        .catch((err) =>
          console.error(
            `Failed to unsubscribe form subscription '${subscriptionId}'`,
            err
          )
        )
  })
}

export { snapshotAndSubscribe }
