import React, { FC, useEffect, useRef } from 'react'

import {
  ColDef,
  GridReadyEvent,
  ColumnRowGroupChangedEvent,
} from 'ag-grid-community'
import { AgGridReact } from 'ag-grid-react'
import { Client } from 'amps'
import { Subscription } from 'rxjs'

import { DataRecord } from './amps'
import { Release } from './types'
import { useCache } from './useCache'

import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

const TOPIC = 'releases'

type Props = {
  client: Client
}

const colDefs: ColDef[] = [
  { field: 'id' },
  { field: 'count' },
  { field: 'state' },
  { field: 'tradeSide' },
  { field: 'fillQuantity' },
  { field: 'releaseQuantity' },
  { field: 'bookedQuantity' },
  { field: 'isViaFix' },
  { field: 'isHidden' },
]

const Grid: FC<Props> = ({ client }) => {
  const [data$, setQuery] = useCache<Release>(client, TOPIC)
  const subscription = useRef<Subscription>()

  useEffect(() => () => subscription.current?.unsubscribe())

  const onGridReady = async ({ api }: GridReadyEvent) => {
    subscription.current?.unsubscribe()

    subscription.current = data$.subscribe((data) => {
      console.log(data)
      data.__reset ? api.setRowData([]) : api.batchUpdateRowData(data)
    })
    setQuery()

    //api.sizeColumnsToFit()

    // const cmd = new Command('sow_and_subscribe')
    // cmd.topic(TOPIC)
    // cmd.options(`
    // oof,
    // conflation=1000ms,
    // grouping=[/state],
    // projection=[
    //     SUM(/fillQuantity) AS /fillQuantity,
    //     SUM(/releaseQuantity) AS /releaseQuantity,
    //     COUNT(/id) AS /count,
    //     /state
    // ]`)

    // subscription<Release>(client, cmd, (r) => [r.__key]).subscribe(
    //   (headers) => {
    //     api.updateRowData(headers)

    //     if (headers.add?.length) {
    //       headers.add.forEach(({ state, __path }) => {
    //         if (state) {
    //           const cmd2 = new Command('sow_and_subscribe')
    //           cmd2.topic(TOPIC)
    //           cmd2.filter(`/state = '${state}'`)
    //           cmd2.options(`oof,conflation=1000ms`)

    //           // subscribe(client, cmd2, (c) => [...__path, c.__key]).subscribe(
    //           //   (leafs) => {
    //           //     api.updateRowData(leafs)
    //           //   }
    //           // )
    //         }
    //       })
    //     }
    //   },
    //   (err) => alert(err)
    // )
  }

  // useEffect(() => console.info('Query', query), [query])

  const onColumnRowGroupChanged = ({ columns }: ColumnRowGroupChangedEvent) => {
    const groupBy = columns?.map((c) => c.getId())
    setQuery({ groupBy: groupBy })
  }

  return (
    <div className="ag-theme-balham" style={{ height: '90vh' }}>
      <AgGridReact
        defaultColDef={{
          sortable: true,
          filter: true,
          resizable: true,
          enableRowGroup: true,
        }}
        onColumnRowGroupChanged={onColumnRowGroupChanged}
        sideBar
        rowData={[]}
        treeData
        enableColResize
        rowGroupPanelShow="always"
        columnDefs={colDefs}
        getRowNodeId={(r: DataRecord<unknown>) => r.__key}
        getDataPath={(r: DataRecord<unknown>) => r.__path}
        onGridReady={onGridReady}
      />
    </div>
  )
}

export default Grid
