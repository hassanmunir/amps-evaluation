import { Client } from 'amps'
import { Subject, Observable } from 'rxjs'
import { startWith, switchMap } from 'rxjs/operators'

import { snapshotAndSubscribe } from './amps'
import { DataTransaction, Reset, Query } from './types'

type Stream<T> = Observable<DataTransaction<T>>

const reset: Reset = { __reset: true }

export const isReset = <T>(obj: Reset | DataTransaction<T>): obj is Reset =>
  !!(obj as Reset).__reset

export const useCache = <T>(
  client: Client,
  topic: string
): [Stream<T>, (query?: Omit<Query, 'topic'>) => void] => {
  const querySubject = new Subject<Query>()

  const setQuery = (query?: Omit<Query, 'topic'>) =>
    querySubject.next({
      topic,
      ...query,
    })

  const stream$: Observable<DataTransaction<T> | Reset> = querySubject.pipe(
    switchMap((query) =>
      snapshotAndSubscribe<T>(client, query).pipe(startWith(reset))
    )
  )

  return [stream$, setQuery]
}
