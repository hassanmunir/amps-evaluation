import React, { useState, useEffect } from 'react'

import 'ag-grid-enterprise'

import { Client, DefaultServerChooser, DefaultSubscriptionManager } from 'amps'

import Grid from './Grid'

const HOST = 'localhost'
const PORT = '9008'

function App() {
  const [client, setClient] = useState<Client>()
  const [error, setError] = useState()

  useEffect(() => {
    const chooser = new DefaultServerChooser()
    chooser.add(`ws://${HOST}:${PORT}/amps/json`)

    const ampsClient = new Client('view-server')
    ampsClient.serverChooser(chooser)
    ampsClient.subscriptionManager(new DefaultSubscriptionManager())

    ampsClient
      .connect()
      .then(() => setClient(ampsClient))
      .catch((e) => setError(e))

    return () => void client?.disconnect()
  }, [])

  return (
    <>
      {!client && <p>Loading...</p>}
      {client && <Grid client={client} />}
      {error && <p>{JSON.stringify(error, null, 2)}</p>}
    </>
  )
}

export default App
