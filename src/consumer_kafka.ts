import { Client, DefaultServerChooser, MemoryPublishStore } from 'amps'
import { KafkaClient, Consumer as KafkaConsumer } from 'kafka-node'

const AMPS_HOST = 'localhost'
const AMPS_PORT = '9008'

const TOPIC = 'releases'

const kafkaHost = 'localhost:9092'

// create the server chooser
const chooser = new DefaultServerChooser()
chooser.add(`ws://${AMPS_HOST}:${AMPS_PORT}/amps/json`)

// create the HA publisher and connect
const client = new Client('releases_publisher')
client.serverChooser(chooser)
client.publishStore(new MemoryPublishStore())
const kafkaClient = new KafkaClient({ kafkaHost })

client
  .connect()
  .then(() => {
    const consumer = new KafkaConsumer(kafkaClient, [{ topic: TOPIC }], {
      autoCommit: false,
      fetchMaxBytes: 1024 * 1024,
      fetchMaxWaitMs: 1000,
      fromOffset: false,
    })

    consumer.on('message', (msg) => {
      if (msg.value === 'null') {
        client.sowDelete(TOPIC, `/id = "${msg.key}"`)
      } else {
        client.publish(TOPIC, msg.value)
      }
    })

    consumer.on('error', (err) => console.log('kafka_error', err))
  })
  .catch((err) => console.log('amps_error', err))
