# Min Requirements

1. Kafka Server running on port `9092`
1. Kafka Topic called `releases`
1. AMPS server accepting WebSocket connections on port `9008`
1. AMPS Topic called `releases`
   - User `config.xml` from `./amps` when starting AMPS server

# In this Repo

1. `npm run publisher:kafka` - Publishes randomly generated data to Kafka topic
1. `npm run consumer:kafka` - Reads messages from Kafka topic and publishes to AMPS
1. `npm run start` - Displays data from AMPS with real time updates
